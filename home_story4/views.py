from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

def homepage(request):
	return render(request, "home_story3.html")

def profileku(request):
	return render(request, "profile_story3.html")

def regisku(request):
	return render(request, "regis_story3.html")
	